
export const Msg = new Mongo.Collection("msgs");

export const MsgSchema = new SimpleSchema({
    name : {
        type: String,
        max: 20
    },
    msg :{
        type: String
    },
    date:{
        type: Date,
        optional: true,
        autoValue: function() {
            if (this.isInsert) {
                return new Date();
            }  else {
                this.unset();  // Prevent user from supplying their own value
            }
        }
    }
});

Msg.attachSchema(MsgSchema);