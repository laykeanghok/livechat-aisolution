import {Msg, MsgSchema} from '../collections/msg';
import {check} from 'meteor/check';

Meteor.methods({
   'chat.insert' (msg1){
       check(msg1, MsgSchema);
       Msg.insert(msg1);
   }
});
