import {Msg} from '../../lib/collections/msg';
Meteor.publish('all_msg', function () {
  return Msg.find({}, {sort: {date: -1}, limit: 10});
});