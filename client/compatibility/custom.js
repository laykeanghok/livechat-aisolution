function chnage_size_screen(){
	var chat_container = $('#chat_container');
	var title_height = $('.title').height();
	var chat_form_height = $('#chat_form').height();
	console.log('title height '+title_height+' chat_form_height '+chat_form_height+' window height '+ $(document).height());
	var available_screen = $(document).height() - (title_height + chat_form_height + 130);
	chat_container.css('max-height', available_screen);
	return available_screen;
}

var chat_form = 0;

function chat_scroll_bottom(){
	console.log($('#chat_container')[0].scrollHeight)
	$('#chat_container').animate({ scrollTop: $('#chat_container')[0].scrollHeight }, "slow");
}

$(document).ready(function() {
	chnage_size_screen();
	chat_form = $('#chat_container')[0].scrollHeight;
	console.log(chat_form);
	chat_scroll_bottom();
});

$(window).on("orientationchange",function(){
  chnage_size_screen();
});