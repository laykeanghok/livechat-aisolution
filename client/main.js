import {Template} from 'meteor/templating';
import {ReactiveVar} from 'meteor/reactive-var';
import {Msg} from '../lib/collections/msg';
Filter = require('bad-words');
filter = new Filter();
filter.addWords(['juy', 'ចុយ', 'ក្ដ', 'kdor', 'ach' , 'អាច៏', 'ក្ដិត' , 'kdet', 'kdit', 'fab', 'fap', 'kdouy' , 'kduy', 'កន្ដួយ']);

import './main.html';
Template.chat_window.onCreated(function () {
    Meteor.subscribe('all_msg');
});

Template.chat_window.onRendered(function () {
});

Template.chat_window.helpers({
    'chats': function () {
        return Msg.find({}, {sort: {date: 1}});
    },
    'format_date': function (date) {
        return moment(date).startOf('sec').fromNow();
    },
    'doneTrigger': function() {
      Meteor.setTimeout(function(){
        chat_scroll_bottom();
      }, 1000);
    return null;
  }
});


Template.chat_window.events({
    'submit #chat_form'(event, instance) {
        event.preventDefault();
        const name = filter.clean(event.target.username.value);
        const msg = filter.clean(event.target.msg.value);
        Meteor.call('chat.insert', {name: name, msg: msg}, function (err) {
            if (err){
                Materialize.toast(err, 4000, 'red');
            }else{
                Materialize.toast('សារត្រូវបានបញ្ជូន!', 4000, 'green');
                event.target.msg.value = "";
                chat_scroll_bottom();
            }
        });

    },

});
